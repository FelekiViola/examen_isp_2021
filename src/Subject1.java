public class Subject1 {
    public static void main(String[] args) {

    }
}

interface Interface1{                                  //I consider that Interface1 defines metA() method
    void metA();
}

class A implements Interface1{
    M m = new M();                                      //class A owns class M via member initialization => composition

    public void metA() {                                //class A realizes Interface1 => interface realization
        System.out.println("Implemented metA");
    }
}

class M{
    B b;                                                //class M has a class B => aggregation

    public void setB(B b) {
        this.b = b;
    }
}

class B{
    public void metB(){}
}

class L{
    private long t;

    M m;                                                 //class L has a reference to class M in the form of an attribute => association

    public void f(){}

    public void doSomething(X x){}                       //class L uses class X => dependency
}

class X{
    public void i(L l){}
}
