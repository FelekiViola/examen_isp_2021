import java.text.SimpleDateFormat;
import java.util.Date;

public class Subject2 {
    public static void main(String[] args) {
        YThread BThread1 = new YThread();
        YThread BThread2 = new YThread();
        YThread BThread3 = new YThread();


        Thread t1 = new Thread(BThread1, "BThread1");
        Thread t2 = new Thread(BThread2, "BThread2");
        Thread t3 = new Thread(BThread3, "BThread3");

        t1.start();
        t2.start();
        t3.start();

    }
}

class YThread implements Runnable{

    public void run(){
        Thread t = Thread.currentThread();
        for(int i = 0; i<7; i++){
            SimpleDateFormat formatter = new SimpleDateFormat(" HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            String msg = t.getName() + " -" + formatter.format(date);
            System.out.println(msg);
            try{
                Thread.sleep(1000); //1 sec
            } catch(InterruptedException e){
                e.printStackTrace();
            }
        }
        System.out.println(t.getName() + " its job is DONE!");

    }
}

